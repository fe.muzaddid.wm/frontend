package com.mp.training.microservice.frontend.dto;


import lombok.Data;

import java.math.BigDecimal;

@Data
public class Product {
    private String id;
    private String code;
    private String name;
    private BigDecimal price;
}